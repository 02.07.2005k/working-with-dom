// Теоретичні питання 
// 1) DOM - це програмний інтерфейс, який дозволяє програмам та 
// скриптам змінювати структуру, стиль та зміст веб-сторінки.
// 2) innerHTML містить HTML-код всередині елемента, тоді як 
// innerText містить тільки текстовий вміст без HTML-розмітки.
// 3) До елемента сторінки можна звернутися за допомогою JavaScript за допомогою 
// методів getElementById, getElementsByClassName, getElementsByTagName, querySelector, querySelectorAll.
// Найкращий спосіб залежить від конкретного використання, 
// але зазвичай вважається, що querySelector і querySelectorAll є більш гнучкими та зручними.
// 4)  nodeList - це колекція елементів, яка повертається методами DOM, такими як querySelectorAll.
// HTMLCollection - це також колекція елементів, але вона повертається методами DOM, 
// такими як getElementsByClassName або getElementsByTagName. 
// Одна з основних різниць полягає в тому, що nodeList може містити будь-які типи вузлів, 
// в той час як HTMLCollection містить лише елементи HTML.



// Завдання 1
// Спосіб 1: 
const featureElements1 = document.querySelectorAll('.feature');
console.log('Знайдені елементи з класом "feature" (спосіб 1):', featureElements1);

// Спосіб 2:
const featureElements2 = document.getElementsByClassName('feature');
console.log('Знайдені елементи з класом "feature" (спосіб 2):', featureElements2);

// Задання кожному елементу з класом "feature" вирівнювання тексту по центру
for (let i = 0; i < featureElements1.length; i++) {
    featureElements1[i].style.textAlign = 'center';
}

// Завдання 2
const h2Elements = document.querySelectorAll('h2');
h2Elements.forEach(element => {
    element.textContent = 'Awesome feature';
});

// Завдання 3
const featureTitleElements = document.querySelectorAll('.feature-title');
featureTitleElements.forEach(element => {
    element.textContent += '!';
});